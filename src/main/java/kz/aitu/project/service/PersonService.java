package kz.aitu.project.service;

import kz.aitu.project.model.Person;
import kz.aitu.project.repository.PersonRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PersonService {
    public final PersonRepository personRepository;

    public PersonService(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    public Person create(Person person) {
        return personRepository.save(person);
    }
    public void delete(Long id) {
        personRepository.deleteById(id);
    }
    public List<Person> getAll() {
        return (List<Person>) personRepository.findAll();
    }
    public Person getById(Long id) {
        return personRepository.findById(id).orElse(null);
    }
    public Person update(Person person) {
        return personRepository.save(person);
    }
}
