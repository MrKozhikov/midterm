package kz.aitu.project.controller;

import kz.aitu.project.model.Person;
import kz.aitu.project.service.PersonService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class PersonController {
    public final PersonService personService;

    public PersonController(PersonService personService) {
        this.personService = personService;
    }

    @PostMapping(path = "/api/v2/users")
    public ResponseEntity<?> savePerson(@RequestBody Person person) {
        return ResponseEntity.ok(personService.create(person));
    }
    @DeleteMapping(path = "/api/v2/users/{id}")
    public void deletePersonById(@PathVariable long id) {
        personService.delete(id);
    }
    @GetMapping(path="/api/v2/users")
    public List<Person> getPerson() {
        return personService.getAll();
    }
    @GetMapping(path = "/api/v2/users/{id}")
    public Person getPerson(@PathVariable long id) {
        return personService.getById(id);
    }

    @PutMapping(path = "/api/v2/users")
    public ResponseEntity<?> updatePerson(@RequestBody Person person) {
        return ResponseEntity.ok(personService.update(person));
    }
}
