create table if not exists "person"
(
	id bigint,
	first_name varchar(255),
	last_name varchar(255),
	city varchar(255),
	phone bigint,
	telegram varchar(255),
    primary key (id)
);
alter table "person" owner to postgres;